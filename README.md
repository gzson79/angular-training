# Training

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.8.

## Docker for the development environment

Docker images could be used to play without installing node, angular or typescript. 

Run `buildDockers.bat`(windows) or `buildDockers.sh`(linux) to build two docker images; one that runs the app in the built-in server, the other one that runs bash shell that has typescript installed.

After all images are built, run them using `runDocker.bat` or `runDocker.sh` with one parameter. <br> 
When s or S is passed, it's running the server image. Or, it runs the terminal that you can try typescript on the fly.

Example:

 * `runDocker.sh s`  // will run server <br>
 * `runDocker.sh`    // will run terminal 

Development folders are mapped between local and container as following.

 * server:&nbsp;&nbsp;&nbsp;&nbsp;     src => /app/src
 * terminal:&nbsp;&nbsp;&nbsp;   terminal => /app/terminal
