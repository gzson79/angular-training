enum BTN_STATUS {ON, OFF};

export class Like {

  constructor(private numOfLikes, private btnStatus) {
    this.numOfLikes = 0;
    this.btnStatus = BTN_STATUS.OFF;
  }

  btnClicked() {
    switch ( this.btnStatus ) {
      case BTN_STATUS.OFF:
        this.numOfLikes++;
        this.btnStatus = BTN_STATUS.ON;
        break;
      case BTN_STATUS.ON:
      default:
        this.numOfLikes--;
        this.btnStatus = BTN_STATUS.OFF;
        break;
    }
  }

  toString() {
    console.log('current status:');
    console.log('Number of likes:' + this.numOfLikes);
    console.log('Button Status:' + this.btnStatus);
  }

}
