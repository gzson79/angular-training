@echo off
cls

set result=false
if "%1" == "s" set result=true
if "%1" == "S" set result=true

if "%result%" == "true" (
    docker run -it --rm -p 4200:4200 -v "./src:/app/src" training
) else (
    docker run -it --rm --name=terminal -v "./terminal:/app/terminal" training-terminal bash
)
