#!/bin/bash

clear

if [[ $1 == 's' ]]  || [[ $1 == 'S' ]]
then
  docker run -it --rm -p 4200:4200 -v $PWD/src:/app/src training
else
  docker run -it --rm -v $PWD/terminal:/app/terminal training-terminal bash
fi
