#!/bin/bash

clear

docker image build -f Dockerfile.server -t training .
docker image build -f Dockerfile.terminal -t training-terminal .
